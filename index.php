<?php 
	require "./static/php/Mobile_Detect.php";
	$device = new Mobile_Detect;
?>
<!doctype html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Olá, somos a Parati. Um estúdio de design de interfaces digitais.</title>

		<meta name="description" content="Criamos interfaces para ambientes digitais. Conheça como trabalhamos, e como potencializar o seu serviço ou negócio através de site ou um app. Dê um alô!">

		<meta property="og:title" content="Olá, somos a Parati" />
		<meta property="og:description" content="Criamos interfaces para ambientes digitais. Conheça como trabalhamos, e como potencializar o seu serviço ou negócio através de site ou um app. Dê um alô!" />
		<meta property="og:url" content="https://parati.ag/" />
		<meta property="og:image" content="https://parati.ag<?php echo str_replace("index.php","",$_SERVER["SCRIPT_NAME"]); ?>static/favicon/og-image.png">
		<meta property="og:site_name" content="Parati User Interface Design Studio" />
		<meta property="og:locale" content="pt_BR" />

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#589264" />

		<link rel="canonical" href="https://parati.ag/">

		<link rel="icon" type="image/png" href="./static/favicon/favicon-196x196.png" sizes="196x196" />
		<link rel="icon" type="image/png" href="./static/favicon/favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="./static/favicon/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="./static/favicon/favicon-16x16.png" sizes="16x16" />


		<!--<link rel="stylesheet" href="./static/css/parati-site.css?v=<?php echo filemtime('./static/css/parati-site.css'); ?>">-->
		<style type="text/css">
			<?php include("./static/css/parati-site.css"); ?>
		</style>

		<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122612668-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-122612668-1');
		</script>
		<?php endif; ?>
	</head>
	<body class="pi-intro">
		<header class="pi-header" id="Inicio">
			<h1 id="pi-hello">Olá, <strong>Somos a Parati.</strong></h1>
			<div class="pi-header-ctt" id="pi-header-ctt">
				<div class="pi-header-logo-block">
					<span class="pi-logo" title="Parati Logo">Parati</span>
				</div>
				<div class="pi-header-title-block">
					<h2>Criamos interfaces para ambientes digitais</h2>
				</div>
			</div>
			<div class="pi-video-bg">
				<?php if(!$device->isMobile()): ?>
					<video autoplay muted loop poster="./static/mp4/video-poster.jpg?v=<?php echo filemtime('./static/mp4/video-poster.jpg'); ?>">
						<source src="./static/mp4/video-site.mp4?v=<?php echo filemtime('./static/mp4/video-site.mp4'); ?>" type="video/mp4">
					</video>
				<?php endif; ?>
			</div>
		</header>

		<div class="pi-nav-trigger-bg"></div>
		<a href="javascript:;" class="pi-nav-trigger" title="Menu">
			<div class="pi-hamburger">
				<div class="pi-line-menu half start"></div>
				<div class="pi-line-menu"></div>
				<div class="pi-line-menu half end"></div>
			</div>
		</a>
		<nav class="pi-nav">
			<ul>
				<li><a href="#Inicio">Início</a></li>
				<li><a href="#O-que-fazemos">O que fazemos</a></li>
				<li><a href="#Como-trabalhamos">Como trabalhamos</a></li>
				<li><a href="#Clientes-e-projetos">Clientes e projetos</a></li>
				<li><a href="#Equipe">Equipe</a></li>
				<li><a href="#Contato">Contato</a></li>
			</ul>
		</nav>

		<section class="pi-what-we-do" id="O-que-fazemos">
			<h2 class="pi-section">O que fazemos</h2>
			<h3>Desenvolvemos interfaces digitais para produtos e serviços sob a perspectiva de quem irá utilizá-lo</h3>

			<div class="pi-ctt">
				<p>O que queremos é que seu cliente execute da melhor forma os objetivos de negócio dentro do contexto digital, seja em um aplicativo de smartphone ou smartwatch, um site responsivo ou uma aplicação para desktop.</p>
				<p>Acreditamos em testar hipóteses! Quanto antes um produto for testado, mais rápido ele se adaptará às necessidade de negócio, de mercado e do público a que se destina.</p>
			</div>
		</section>

		<section class="pi-how-we-work" id="Como-trabalhamos">
			<h2 class="pi-section">Como trabalhamos</h2>
			<h3>
				Buscar<span>,</span><br>
				Fazer<span> e </span><br>
				Testar
			</h3>
			<div class="pi-ctt">
				<p>A base do nosso processo é trabalhar em conjunto com o cliente para encontrar soluções a partir de pesquisas e análises. Isso é feito num prazo mínimo possível para que as ideias iniciais se transformem em um protótipo e assim se possa avaliar as soluções num formato mais próximo do real. Essa agilidade nos permite captar lições de outros validadores de negócio e, também, dos próprios utilizadores da interface.</p>
			</div>
			<h4>Como podemos <span>apoiar o seu projeto</span></h4>
			<div class="pi-cards">
				<div class="pi-card" id="pi-card-1">
					<div>
						<h5>
							Tudo<br>
							novo de<br>
							novo
						</h5>
						<p>
							Tem um projeto novo na cabeça? Nós te ajudamos a colocá-lo no ar, desde pesquisa até a publicação. Ou, se você já tem uma versão do seu produto rodando mas busca algo novo e totalmente reformulado, também podemos entrar nessa jogada e te dar todo apoio para essa mudança.
						</p>
					</div>
				</div>
				<div class="pi-card" id="pi-card-2">
					<div>
						<h5>
							Melhorias de usabilidade
						</h5>
						<p>
							Para quem procura ajustes pontuais na arquitetura, no layout e melhorias na usabilidade, esse é o caminho. Este tipo de prestação de serviço é ideal para quem identifica que o usuário ainda sente dificuldades ao usar o site ou app.
						</p>
					</div>
				</div>
				<div class="pi-card" id="pi-card-3">
					<div>
						<h5>
							Somente prototipação
						</h5>
						<p>
							Se você já tem outro fornecedor criando a interface gráfica e precisa de apoio especializado em Front-end para atuar junto à equipe de TI, fale com a gente. Temos o pacote específico para esse caso.
						</p>
					</div>
				</div>
				<div class="pi-card" id="pi-card-4">
					<div>
						<h5>
							Consultoria mensal
						</h5>
						<p>
							Essa é a nossa prestação de serviço a longo prazo. É ideal para quem tem um site ou serviço que demanda melhorias e correções constantes. Disponibilizamos um time de Design de Interfaces e Conteúdo para acompanhamento e consultoria mensal do seu produto.
						</p>
					</div>
				</div>
			</div>
		</section>

		<section class="pi-clients" id="Clientes-e-projetos">
			<h2 class="pi-section">Clientes e projetos</h2>
			<div class="pi-cards">
				<div class="pi-card pi-card-selected">
					<h3><a href="javascript:;">B3</a></h3>
					<div class="pi-ctt">
						<span class="pi-logo-b3" title="B3 - Bolsa Brasil Balcão"></span>
						<p>
							Apoiamos o desenvolvimento do Design System para a B3. Este tipo de recurso possibilita estabelecer e divulgar os mesmos padrões da empresa para todos os seus produtos digitais da empresa, fazendo com que toda a cadeia de envolvidos (designers, profissionais de marketing, desenvolvedores) utilize os mesmos padrões, economizando recursos e aumentando a produtividade para elaboração de produtos digitais.
						</p>
						<p>
							<a href="https://parati.ag/static/mp4/B3-DS-Overview-Web.mp4" target="_blank" class="pi-btn">Case do Design System da B3</a>
						</p>
					</div>
				</div>
				<div class="pi-card">
					<h3><a href="javascript:;">Porto</a></h3>
					<div class="pi-ctt">
						<span class="pi-logo-porto" title="Porto"></span>
						<p>
							A parceria com a Porto vem desde 2013 e temos desenvolvido diversos produtos digitais para as diferentes empresas dentro da companhia.
						</p>
						<p>
							O desenvolvimento das interfaces para a <em>contratação de seguros de forma totalmente online</em> também é um ponto alto dessa parceria, além das <em>Landing Pages</em> para campanhas específicas. 
						</p>
						<p>
							<a href="https://www.behance.net/paratiuid" target="_blank" class="pi-btn">Veja nosso portfólio</a>
						</p>
					</div>
				</div>
				
				<div class="pi-card">
					<h3><a href="javascript:;">SciELO</a></h3>
					<div class="pi-ctt">
						<span class="pi-logo-scielo" title="SciELO"></span>
						<p>
							A SciELO é uma biblioteca eletrônica que indexa e publica periódicos e artigos científicos em acesso aberto, ou seja, disponibiliza e dissemina informação científica de forma gratuita. Estamos nesta parceria desde 2013, apoiando a criação de interfaces para os sistemas internos e as extranets. Também trabalhamos no desenvolvimento de novas interfaces para os portais internacionais e nacionais.
						</p>
						<p>
							<a href="https://www.behance.net/paratiuid" target="_blank" class="pi-btn">Veja nosso portfólio</a>
						</p>
					</div>
				</div>
			</div>
		</section>
		
		<?php /*
		<section class="pi-team" id="Equipe">
			<h2 class="pi-section">Equipe</h2>
			<div class="pi-team-list">
				<div class="pi-team-member" id="pi-team-lc">
					<h3>Luiz <strong>Caramez</strong></h3>
					<span>UX Designer &amp; Fundador</span>
				</div>
				<div class="pi-team-member" id="pi-team-mf">
					<h3>Maria Fernanda <strong>Moraes</strong></h3>
					<span>Copywriter</span>
				</div>
				<div class="pi-team-member" id="pi-team-er">
					<h3>Eduardo <strong>Robles</strong></h3>
					<span>Front-end developer</span>
				</div>
				<div class="pi-team-member" id="pi-team-rc">
					<h3>Ramon <strong>Cordini</strong></h3>
					<span>UX/UI Designer</span>
				</div>
				<div class="pi-team-member" id="pi-team-sp">
					<h3>Sergio <strong>Pádua</strong></h3>
					<span>UX/UI Designer</span>
				</div>
			</div>
		</section>
		*/ ?>

		<footer class="pi-footer" id="Contato" itemscope itemtype="http://schema.org/LocalBusiness">
			<h2 class="pi-section">Contato</h2>
			<div class="pi-ctt">
				<h3>Dê um alô!</h3>
				<p>Fale com a gente. O papo pode render um projeto novo ou, quem sabe, uma parceria. Currículos também são bem-vindos ;)</p>
				<a href="mailto:alo@parati.ag" class="pi-mail pi-btn" itemprop="email">alo@parati.ag</a>
				<a href="tel:(48) 3206.0878" class="pi-btn pi-btn-phone" itemprop="telephone"><span class="pi-phone" title="Ligue-nos"></span> <span class="pi-number">48 3206.0878</span></a>
			</div>
			<div class="pi-credentials">
				Quer conhecer um pouco mais sobre nosso jeito de trabalhar?<br>
				<a href="./static/pdf/Parati-Credenciais.pdf" target="_blank">Baixe nossa apresentação</a>
			</div>
			<div class="pi-addr">
				<strong itemprop="name">Parati User Interface Design Studio</strong>
				<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<span itemprop="addressLocality">Florianópolis</span>/
					<span itemprop="addressRegion">SC</span>
				</p>
			</div>
		</footer>

		<script data-main="./static/js/pi-boot-min" src="./static/vendor/requirejs/require.min.js"></script>

		<script>
			(function(d) {
				var config = {
					kitId: 'sap8cns',
					scriptTimeout: 3000,
					async: true
				},
				h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
			})(document);
		</script>
	</body>
</html>

