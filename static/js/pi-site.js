var _pi = {};

_pi.PageDestroy = function() {
	_pi.Carrossel(true);
	_pi.HowWeWork(true);
};

_pi.PageInit = function(secondTime) {
	if(typeof secondTime == "undefined") secondTime = false;

	if(!secondTime) {
		$("#pi-hello").html("").addClass("pi-visible");
		_pi.Hello();

		if($(window).scrollTop() > 0)
			$("body").removeClass("pi-intro");

	}
	_pi.Carrossel();
	_pi.HowWeWork();
	_pi.Clients();

	_pi.MenuShowHide();
	_pi.MenuTrigger();
};

_pi.Hello = function() {
	if(window.pi_isMobile) {
		var instance = new TypeIt("#pi-hello", {
								speed: 100,
								autoStart: true,
								callback: _pi.AfterIntro
							})
							.type("Olá!")
							.pause(1500)
							.delete(1)
							.type(",")
							.type(" <strong>somos a Parati.</strong>");
	} else {
		var instance = new TypeIt("#pi-hello", {
							speed: 100,
							autoStart: true,
							callback: _pi.AfterIntro
						})
						.type("Olá!")
						.pause(1500)
						.delete(1)
						.type(",")
						.break()
						.type(" <strong>Somos a Parati.</strong>");
	}
};

_pi.AfterIntro = function() {
	$("#pi-hello").addClass("pi-ti-hideCursor");
	$("#pi-header-ctt").addClass("pi-activated");
	$("#O-que-fazemos").addClass("pi-activated");
	$("body").removeClass("pi-intro");
};

_pi.Carrossel = function(destroy) {
	if(typeof destroy == "undefined") destroy = false;

	var piCards = $(".pi-cards"),
		piTeamList = $(".pi-team-list"),
		piHowWeWork = $(".pi-how-we-work .pi-cards");

	if(!destroy) {
		if(window.pi_isMobile) {

			_pi.startCarrossel(piCards,{
				slidesToScroll: 1,
				dots: false,
				centerMode: true,
				centerPadding: '3rem',
				arrows: true,
				mobileFirst: true,
				infinite: false,
				responsive: [
					{
						breakpoint: 767,
						settings: "unslick"
					}
				]
			});
			_pi.startCarrossel(piTeamList,{
				slidesToScroll: 1,
				dots: false,
				centerMode: true,
				centerPadding: '0',
				arrows: true,
				mobileFirst: true,
				infinite: true,
				responsive: [
					{
						breakpoint: 767,
						settings: "unslick"
					}
				]
			});
		} else if(window.pi_isTablet) {
			_pi.startCarrossel(piHowWeWork,{
				slidesToScroll: 1,
				slidesToShow: 1,
				dots: false,
				centerMode: true,
				centerPadding: '10rem',
				arrows: true,
				mobileFirst: true,
				infinite: false,
				responsive: [
					{
						breakpoint: 1024,
						settings: "unslick"
					}
				]
			});
		}
	} else {
		_pi.DisableCarrossel($(".pi-cards, .pi-team-list"));
	}
};

_pi.DisableCarrossel = function(obj) {
	obj.each(function() {
		var t = $(this);

		if(t.is(".slick-initialized")) {
			$(this).unslick();
		}
	});
};

_pi.startCarrossel = function(obj,cfg) {
	if(!obj.is(".slick-activated")) {
		obj.slick(cfg);
	}
}

_pi.MenuShowHide = function() {
	window.lastScrollPos = 0;

	$(window).off("scroll.piScroll").on("scroll.piScroll",function() {
		var _top = $(this).scrollTop(),
			_class = "activated";

		if (_top < (window.lastScrollTop) && _top > 100) {
			$(".pi-nav-trigger-bg, .pi-nav-trigger").addClass(_class);
		} else {
			$(".pi-nav-trigger-bg, .pi-nav-trigger").removeClass(_class);
		}

		window.lastScrollTop = _top;
	});
}

_pi.MenuTrigger = function() {
	var nav = ".pi-nav",
		trigger = ".pi-nav-trigger";

	$(trigger).off("click.piTrigger").on("click.piTrigger",function(e) {
		e.preventDefault();

		var t = $(this),
			closedClass = "pi-nav-closed",
			opened = t.is("."+closedClass) ? true : false;

		if(!opened) {
			t.addClass(closedClass);
			$(nav).addClass("activated");
			$("body").addClass("pi-menu");
		} else {
			t.removeClass(closedClass);
			$(nav).removeClass("activated");
			$("body").removeClass("pi-menu");
		}
	});

	$("a",nav).off("click.piTrigger").on("click.piTrigger",function(e) {
		e.preventDefault();

		var t = $(this),
			dest = t.attr("href"),
			pos = $(dest).offset().top;

		$(nav).removeClass("activated");
		$(trigger).removeClass("pi-nav-closed");
		setTimeout(function() {
			$("html,body").animate({
				scrollTop: pos
			},400,function() {
				$("body").removeClass("pi-menu");
			});
		},400);
	});
};

_pi.HowWeWork = function(destroy) {
	if(typeof destroy == "undefined") destroy = false;

	if(!destroy) {
		if(!(window.pi_isMobile || window.pi_isTablet)) {
			$("#pi-card-1, #pi-card-2, #pi-card-3, #pi-card-4").hover3d({
				selector: "div",
				sensitivity: 25
			});
		}
	} else {
		$("#pi-card-1, #pi-card-2, #pi-card-3, #pi-card-4").off("mouseenter mousemove mouseleave").each(function() {
			$(this).removeClass("hover-3d").attr("style","");
		});
	}
};

_pi.Clients = function() {
	if(!window.pi_isMobile) {
		$(".pi-clients .pi-card a").off("click.piClick").on("click.piClick",function() {
			var t = $(this),
				thisCard = t.parents(".pi-card"),
				allCards = t.parents(".pi-cards").find(".pi-card"),
				selectedClass = "pi-card-selected";

			allCards.removeClass(selectedClass);
			thisCard.addClass(selectedClass);
		});
	}
};