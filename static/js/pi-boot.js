function startScreens() {
	window.pi_isMobile = window.innerWidth < 768 ? true : false;
	window.pi_isTablet = !window.pi_isMobile && window.innerWidth < 1023 ? true : false;
}

function setInterfaceScripts() {
	var cfg = {
		"baseUrl": "./static/",
		"paths": {
			"jquery": "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min",
			"hover3d": "./vendor/hover3d/jquery.hover3d.min",
			"slick": "./vendor/slick/slick.min",
			"typeit": "./vendor/typeit/typeit.min",
			"site": "./js/pi-site-min",
		},
		"shim": {
			"site": ["typeit","slick"],
			"hover3d": ["jquery"],
			"slick": ["jquery"],
			"typeit": ["jquery"]
		}
	};

	if(window.pi_isMobile || window.pi_isTablet) {
		cfg.shim.site = ["typeit","slick"];
	} else {
		cfg.shim.site = ["typeit","hover3d"];
	}

	return cfg;
}

startScreens();
var cfg = setInterfaceScripts(),
	waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

console.log(cfg);

// Configs
requirejs.config(cfg);

define(["site"],function($) {
	_pi.PageInit();

	window.addEventListener("resize",function() {
		window.waitForFinalEvent(function(){
			startScreens();

			if(
				(window.pi_isMobile || window.pi_isTablet) &&
				!require.defined("slick")
			) {
				requirejs(["slick"],function($) {
					console.log("carregou o slick");
					_pi.PageDestroy();
					_pi.PageInit(true);
				});
			} else if(
				!(window.pi_isMobile || window.pi_isTablet) &&
				!require.defined("hover3d")
			) {
				requirejs(["hover3d"],function($) {
					console.log("carregou o hover3d");
					_pi.PageDestroy();
					_pi.PageInit(true);
				});
			}
		}, 500, "");
	},false);
});